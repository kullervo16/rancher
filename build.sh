#!/usr/bin/env bash
docker build -t kullervo16/echo:latest .
docker push kullervo16/echo:latest

docker build -f Dockerfile-client -t kullervo16/echo-client:latest .
docker push kullervo16/echo-client:latest
