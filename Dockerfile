FROM openjdk:8
ENTRYPOINT ["java","-jar","/opt/echo.jar"]
EXPOSE 8080
ADD ./build/libs/echo-1.0-SNAPSHOT.jar /opt/echo.jar
