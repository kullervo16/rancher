package kullervo16.rest;


import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class Client {
    public static void main(String args[]) throws InterruptedException {
        String url = System.getenv("URL");
        Integer sleep = Integer.valueOf(System.getenv("SLEEP"));
        System.out.println("-------------------------------------");
        System.out.println("Starting on "+url+" with sleep "+sleep);
        System.out.println("-------------------------------------");
        long startTs = System.currentTimeMillis();
        long count = 0;
        long errorCount = 0;
        double avgRoundTrip = 0.0;
        long maxRT = 0;
        while (true) {
            try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
                while (true) {
                    long sum = count + errorCount;
                    if (sum > 0 && sum % 100 == 0) {
                        double duration = (System.currentTimeMillis() - startTs) / 1000.0;

                        System.out.println(count + " messages " + (100 / duration) + " msg/s. " + errorCount + " errors. Avg RT= " + (avgRoundTrip / 100) + "ms, max RT= " + maxRT + "ms");
                        startTs = System.currentTimeMillis();
                        avgRoundTrip = 0.0;
                        maxRT = 0;
                    }

                    HttpGet httpget = new HttpGet(url + "?timestamp=" + System.currentTimeMillis());
                    //System.out.println("Executing request " + httpget.getRequestLine());

                    // Create a custom response handler
                    ResponseHandler<String> responseHandler = response -> {
                        int status = response.getStatusLine().getStatusCode();
                        if (status >= 200 && status < 300) {
                            HttpEntity entity = response.getEntity();
                            return entity != null ? EntityUtils.toString(entity) : null;
                        } else {
                            throw new ClientProtocolException("Unexpected response status: " + status);
                        }
                    };
                    Thread.sleep(sleep);
                    String responseBody = httpclient.execute(httpget, responseHandler);
                    count++;
                    //                System.out.println("----------------------------------------");
                    //                System.out.println(responseBody);
                    long roundTrip = System.currentTimeMillis() - Long.valueOf(responseBody);
                    avgRoundTrip += roundTrip;
                    if (roundTrip > maxRT) {
                        maxRT = roundTrip;
                    }
                }

            } catch(Exception e){
                errorCount++;
                Thread.sleep(sleep);
            }
        }
    }
}
