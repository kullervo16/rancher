package kullervo16.rest;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class Controller {

    private static final String template = "Hello, %s %s %s!";
    private final AtomicLong counter = new AtomicLong();
    private long lastLog;

    @RequestMapping("/echo")
    public Long echo(@RequestParam(name = "timestamp") Long timestamp, HttpServletRequest request) {

        this.counter.incrementAndGet();

        // echo back the timestamp to allow round-trip calculation
        return timestamp;
    }


    @Scheduled(fixedRate = 10000L)
    public void writeStats(){
        System.out.println("Stats : "+(counter.getAndSet(0)*1000.0/(System.currentTimeMillis()-lastLog))+" msg/s");
        this.lastLog = System.currentTimeMillis();
    }

}
